import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import authReducer from '../reducers/auth';
import cartReducer from '../reducers/cart';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

//Create store
export default (persistedState) => {
    const store = createStore(
        //combineReducers is key value pair of "state value as key" and "reducer as value" of multiple reducers
        combineReducers({
            auth: authReducer,
            cart: cartReducer
        }),
        persistedState,
        //middleware is used to dispatch an action with 'function' without that you can dispatch
        //an action with 'object' only
        composeEnhancers(applyMiddleware(thunk))
        //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );
    return store;
}; 