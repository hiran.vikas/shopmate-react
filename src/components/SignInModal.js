import React from 'react';
import {connect} from 'react-redux';
import {signInApi} from "../services/authService";
import {history} from '../routers/AppRouter';
import {setUserInfo} from '../actions/auth';

class SignInModal extends React.Component {
    

    isValidEmail = (emailField) => {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    
        if (reg.test(emailField) === false) 
        {
            alert('Invalid Email Address');
            return false;
        }
    
        return true;
    
    }

    callSignInApi = (e) => {
        e.preventDefault();
        var email = document.getElementById("email").value;
        var pwd = document.getElementById("password").value;

        if(!this.isValidEmail(email)){
            return;
        }
        if(!pwd){
            alert('Please enter password!');
            return;
        }

        signInApi(email, pwd)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("Response signInApi: ", res);  
            this.props.setUserInfo(res); 
            $('#signInModal').modal('toggle');
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') ("signInApi error", e.response);
            if(e.response.data && e.response.data.message){ 
                alert(e.response.data.message);
                return false;
            }
        });
    }

    render(){
        return(
            <div className="modal fade" id="signInModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header border-bottom-0 py-0">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="container">
                                <div className="row">
                                    <div className="col-12 text-center">
                                        <h5 className="mt-2">Log into Shopmate</h5>
                                        <form className="col-12 text-left"
                                            onSubmit={this.callSignInApi}>
                                            <div className="form-group">
                                                <small>Email</small>
                                                <input type="email" className="form-control" placeholder="Email address" id="email"/>
                                            </div>
                                            <div className="form-group">
                                                <small>Password</small>
                                                <input type="password" className="form-control" placeholder="Enter password" id="password"/>
                                            </div>
                                            <div className="form-group">
                                                <button type='submit' 
                                                    className="btn btn-danger col-12"
                                                    style={{background: "#f52f5e"}}>Log in</button>
                                            </div>
                                            <div className="form-group text-center">
                                                <small className="text-muted">Don't have an account?
                                                    <a className="text-primary font-weight-bold pointer"
                                                        data-dismiss="modal"
                                                        data-toggle="modal"
                                                        data-target="#signUpModal"> Sign up
                                                    </a>
                                                </small>
                                            </div>
                                        </form>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                     </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        //config: state.config
        //auth: state.auth
    }    
};

const mapDispatchToProps = (dispatch) => ({
    setUserInfo: (res) => dispatch(setUserInfo(res)),
});

//connect fn's parameter are for test cases
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(SignInModal);

export default ConnectedComponent;