import React from 'react';
import {connect} from 'react-redux';
import Header from './Header';
import Footer from './Footer';
import {history} from '../routers/AppRouter';

export class DashBoard extends React.Component {
    constructor(props){
        super(props);   
    }

    render() {
        return (
            <div>
                <Header />
                <div style={{height: "91vh"}}>
                    <div className="container-fluid">
                        <div className='row bg-image'>
                            <div style={{marginTop: "70px", marginLeft: "70px"}}>
                                <h5 className="text-light" style={{fontSize: "40px"}}>Background <span style={{fontSize: "35px", fontWeight: "300"}}>and</span> developement</h5>
                                <h5 className="mt-4 text-light col-10 col-md-7 p-0" style={{fontSize: "35px"}}>
                                    Convergent the dictates of the consumer: background and development
                                </h5>
                                <button type="button" 
                                    className="btn btn-light px-5 mt-2" 
                                    style={{color: "#f52f5e", fontWeight: "500"}}
                                    onClick={() => {
                                        history.push({
                                            pathname: `/products/${1}`,
                                            state: { category_id: 1 }
                                        });
                                    }}>View All</button>
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid">
                        <div className="row mt-3">
                            <div className="col-md-4 ml-auto">
                                <div className="pointer text-center" style={{height: "300px", backgroundColor: "#84e6f2"}}>
                                    <div style={{paddingTop: "25%"}}>
                                        <h5 style={{fontSize: "25px"}}>WOW</h5>
                                        <p className="text-danger" style={{fontSize: "18px", fontWeight: "500"}}>Check What!</p>
                                    </div>
                                </div>
                                <div className="mt-4 pointer bg-home text-center" style={{height: "300px"}}>
                                    <h5 className="text-light" style={{fontSize: "25px", paddingTop: "28%"}}>MEN</h5>
                                </div>
                            </div>
                            <div className="col-md-8 mr-auto pl-2 text-center">
                                <img src="/images/gamebegin.png" className="col-md-12 p-0" style={{height: "450px", objectFit: "cover"}}></img>
                                <h5 className="text-center mt-2" style={{fontSize: "35px", wordSpacing: "3px"}}>Let the Game begin</h5>
                                <h5 className="text-center mt-2" style={{fontSize: "15px", fontWeight: "600"}}>Registration is on - get ready for the open</h5>
                                <button type="button" className="btn px-5 mt-2 text-light rounded" style={{background: "#f52f5e", fontWeight: "500"}}>Register</button>
                            </div>
                        </div>
                    </div>

                    <div className="container-fluid mt-5 mb-4 py-5 text-center" style={{background: "#efefef"}}>
                        <h5 style={{color: "#f52f5e", fontWeight: "700"}}>10% Discount for your subscription</h5>
                        <h5 className="col-6 ml-auto mr-auto">Carry the day in style with this extra-large tots crafted in our chic B.B. Collection textured PVC. Featuring colorful faux leather trim, this tots offers a roomy interior.</h5>
                        <div className="row mt-3">
                            <input type="email" className="col-4 ml-auto" placeholder="Your e-mail here!"/>
                            <button type="button" className="btn px-4 text-light rounded mr-auto ml-2" style={{background: "#f52f5e", fontWeight: "500"}}>Subscribe</button>
                        </div>
                    </div>
                    <Footer />
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        //config: state.config,
        //auth: state.auth
    };
};

const mapDispatchToProps = (dispatch) => ({
    //setConfigInfo: (config) => dispatch(setConfigInfo(config)),
});

//connect fn's parameter are for test cases
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(DashBoard);

export default ConnectedComponent;
