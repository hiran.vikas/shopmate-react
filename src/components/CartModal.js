import React from 'react';
import {connect} from 'react-redux';
import {history} from '../routers/AppRouter';
import {updateCartItemsApi, removeItemFromCartApi} from "../services/categories";

class CartModal extends React.Component {
    
    
    updateCartItems = (item) => {
        updateCartItemsApi(item)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("updateCartItemsApi response", res);
            this.setState({cartItems: res});
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.error)
        });
    }

    removeItemFromCart = (item) => {
        removeItemFromCartApi(item)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("updateCartItemsApi response", res);
            const items = this.props.cartItems.filter(function(iitem) { 
                return item.item_id !== iitem.item_id
            })
            console.log("items", items);
            this.props.updateCartItems(items);
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.error);
        });
    }

    render(){
        return(
            <div className="modal fade" id="cartModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header border-bottom-0 py-0">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="container">
                                <div>
                                  <h5 style={{fontSize: "16px"}}>{this.props.cartItems.length} {this.props.cartItems.length > 1? "Items" : "Item"} In Your Cart</h5>
                                  <div className="mt-2">
                                    {
                                        this.props.cartItems.map((item) => {
                                            return (
                                                <div key={item.item_id} className="mt-3">
                                                    <div className="row">
                                                        <img src={`https://raw.githubusercontent.com/zandoan/turing-frontend/master/Images/product_images/${item.image}`}
                                                            style={{width: "50px", height: "50px"}}/>
                                                        
                                                        <div className="ml-3 col-5">
                                                            <p className="mb-0">{item.name}</p>
                                                            <small className="text-primary pointer"
                                                                onClick={() => {
                                                                    this.removeItemFromCart(item);
                                                                }}>x Remove</small>
                                                        </div>
                                                        <small className="text-left my-auto" 
                                                            style={{fontSize: "16px"}}>{item.attributes}</small>
                                                        <div className="row ml-3">    
                                                            <i className="fa fa-minus-circle text-center my-auto ml-3 pointer" 
                                                                style={{fontSize:"24px"}}
                                                                disabled={item.quantity <= 1 ? true : false}
                                                                onClick={() => {
                                                                    if(item.quantity > 1){
                                                                        item.quantity = --item.quantity;
                                                                        //this.props.addItemQuantity(item);
                                                                        this.updateCartItems(item);
                                                                    }
                                                                    else alert("You can not remove");
                                                                }}></i>
                                                            <small className="ml-2 text-center my-auto">{item.quantity}</small>
                                                            <i className="fa fa-plus-circle text-center my-auto ml-3 pointer" 
                                                                style={{fontSize: "24px"}}
                                                                onClick={() => {
                                                                    item.quantity = ++item.quantity;
                                                                    //this.props.removeItemQuantity(item);
                                                                    this.updateCartItems(item);
                                                                }}></i>
                                                        </div>
                                                        <small className="text-left my-auto ml-5" 
                                                            style={{fontSize: "16px"}}>${item.subtotal}</small>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                  </div>
                                  <div className="row mt-5 mx-auto">
                                    <button className="btn rounded px-3 mr-auto border" style={{background: "white", fontWeight: "500", color: "#f52f5e"}}>Back To Shop</button>
                                    <button className="btn rounded px-3 text-light ml-auto" style={{background: "#f52f5e", fontWeight: "500"}}>Apply</button>
                                </div>
                                </div>                                
                            </div>
                        </div>
                     </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        //config: state.config
        //auth: state.auth
    }    
};

const mapDispatchToProps = (dispatch) => ({
    addItemQuantity: (item) => dispatch(addItemQuantity(item)),
    removeItemQuantity: (item) => dispatch(removeItemQuantity(item))
});


//connect fn's parameter are for test cases
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(CartModal);

export default ConnectedComponent;