import React from 'react';
import {connect} from 'react-redux';
import {history} from '../routers/AppRouter';
import {signupApi} from "../services/authService";
import {setUserInfo} from '../actions/auth';

class SignUpModal extends React.Component {
    constructor(props){
        super(props);

    }

    isValidEmail = (emailField) => {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    
        if (reg.test(emailField) === false) 
        {
            alert('Invalid Email Address');
            return false;
        }
    
        return true;
    
    }

    callSignUpApi = (e) =>{
        e.preventDefault();
        var email = document.getElementById("email1").value;
        var pwd = document.getElementById("password1").value;
        var name = document.getElementById("name1").value;

        if(!this.isValidEmail(email)){
            return;
        }

        if(!pwd){
            alert('Please enter password!');
            return;
        }

        if(pwd.length<=4){
            alert('Password length should be atleast 5!');
            return;
        }

        signupApi(name, email, pwd)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("Response signupApi: ", res);
            this.props.setUserInfo(res);
            $('#signUpModal').modal('toggle');
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log("signupApi error", e.response);
            if(e.response.data && e.response.data.msg) alert(e.response.data.msg);
        });
    }

    render(){
        return(
            <div className="modal fade" id="signUpModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header border-bottom-0 py-0">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-12 text-center">
                                        <h5 className="mt-2">Register to Shopmate</h5>
                                        <form className="col-12 text-left"
                                            onSubmit={this.callSignUpApi}>
                                            <div className="form-group">
                                                <small>Name</small>
                                                <input type="text" className="form-control" placeholder="Name" id="name1"/>
                                            </div>
                                            <div className="form-group">
                                                <small>Email</small>
                                                <input type="email" className="form-control" placeholder="Email address" id="email1"/>
                                            </div>
                                            <div className="form-group">
                                                <small>Password</small>
                                                <input type="password" className="form-control" placeholder="Enter password" id="password1"/>
                                            </div>
                                            <div className="form-group">
                                                <button type="submit" 
                                                    className="btn btn-danger col-12"
                                                    style={{background: "#f52f5e"}}
                                                    >Register</button>
                                            </div>
                                            <div className="form-group text-center">
                                                <small className="text-muted">Already have an account?
                                                    <a className="text-primary font-weight-bold pointer"
                                                        data-dismiss="modal"
                                                        data-toggle="modal" 
                                                        data-target="#signInModal"> Log in
                                                    </a>
                                                </small>
                                            </div>
                                        </form>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                     </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        //config: state.config
    }    
};

const mapDispatchToProps = (dispatch) => ({
    setUserInfo: (res) => dispatch(setUserInfo(res)),
});


//connect fn's parameter are for test cases
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(SignUpModal);

export default ConnectedComponent;