import React from 'react';
import {connect} from 'react-redux';

export class Footer extends React.Component {
    constructor(props){
        super(props);   
    }

    render(){   
        return(
            <div className="container-fluid">
                <div className="row">
                    <div className="col-3 px-5">
                        <p className="font-weight-bold mb-0">QUESTIONS?</p>
                        <ul className="list-unstyled">
                            <li><small className="pointer">Help</small></li>
                            <li><small className="pointer">Track Order</small></li>
                            <li><small className="pointer">Returns</small></li>
                        </ul>
                    </div>
                    <div className="col-3 px-5">
                        <p className="font-weight-bold mb-0">WHAT'S IN STORE</p>
                        <ul className="list-unstyled"><li><small className="pointer">Women</small></li>
                            <li><small className="pointer">Men</small></li>
                            <li><small className="pointer">Product A-Z</small></li>
                            <li><small className="pointer">Buy Gift Vouchers</small></li>
                        </ul>
                    </div>
                    <div className="col-3 px-5">
                        <p className="font-weight-bold mb-0">FOLLOW US</p>
                        <ul className="list-unstyled">
                            <li><small className="pointer">Facebook</small></li>
                            <li><small className="pointer">Twitter</small></li>
                            <li><small className="pointer">YouTube</small></li>
                        </ul>
                    </div>
                    <div className="col-3 px-5">
                        <p className="font-weight-bold">&copy;2016 Shopmate Ltd</p>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        //config: state.config,
        //auth: state.auth
    }    
};

const mapDispatchToProps = (dispatch) => ({
});

//connect fn's parameter are for test cases
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(Footer);

export default ConnectedComponent;