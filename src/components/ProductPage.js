import React from 'react';
import {connect} from 'react-redux';
import Header from './Header';
import CartModal from './CartModal';
import SignInModal from './SignInModal';
import SignUpModal from './SignUpModal';
import {history} from '../routers/AppRouter';
import {setCartId} from '../actions/cart';

import {getProductListByCategory, 
        getAttributes, 
        getAttributesValues,
        generateCardId,
        addToCartApi,
        getCartListItems} from "../services/categories";

export class ProductPage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            products: [],
            attributes: [],
            sizes: [],
            colors: [],
            cartId: undefined,
            cartItems: []
        }   
    }

    componentDidMount() {
         console.log("***BASE_API_URl: ", process.env.BASE_API_URL);
        const category_id = this.props.location.state ? this.props.location.state.category_id : undefined
        const name = this.props.location.state ? this.props.location.state.category_name : undefined
        const dept_name = this.props.location.state ? this.props.location.state.department_name : undefined
        
        this.setState({name: name, dept_name: dept_name});

        getProductListByCategory(category_id ? category_id : 1)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("getProductListByCategory", res);
            this.setState({products: res.rows});

            this.callToGetAttributes();
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        });
    }


    callToGetAttributes = () => {
        getAttributes()
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("getAttributes", res);
            this.setState({attributes: res});
            this.callToGetAttributeValues1();
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        });
    }

    callToGetAttributeValues1 = () => {
        //size attribute values
        getAttributesValues(this.state.attributes[0].attribute_id)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("getAttributesvalues size", res);
            this.setState({sizes: res});
            this.callToGetAttributeValues2();
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        });
    }

    callToGetAttributeValues2 = () => {
        //color attribute values
        getAttributesValues(this.state.attributes[1].attribute_id)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("getAttributesValues color", res);
            this.setState({colors: res});
            console.log("cartId: ", this.props.cart.cartId);
            if(!this.props.cart.cartId) this.callToGetCartId();
            else this.callToGetCartList(this.props.cart.cartId);
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        });
    }

    callToGetCartId = () => {
        generateCardId()
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("generateCardId", res);
            this.props.setCartId(res.cart_id);

            this.setState({cartId: res.cart_id}, () => {
                this.callToGetCartList(this.state.cartId);
            });
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        });
    }

    callToGetCartList = (cartId) => {
        getCartListItems(cartId)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("getCartListItems", res);
            this.setState({cartItems: res});
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        });
    }

    /* addToCart = (product) => {
        addToCartApi(this.state.cartId, product.product_id, this.state.sizes[1].value)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("addToCartApi response", res);
            this.setState({cartItems: res});
            alert("Item Added to Cart!");
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.error)
        });
    } */

    render() {
        return (
            <div style={{background: "#efefef"}}>
                <Header cartItems={this.state.cartItems}/>
                <div style={{height: "91vh"}}>
                    <div className="container-fluid">
                        <div className="row py-3" style={{background: "#efefef"}}>
                            <div className="col-md-3 ml-auto">
                                {this.state.name && this.state.dept_name && <div className="card mt-3 p-3" style={{background: "#fafafa"}}>
                                    <h5 className="text-dark">Filter Items</h5>
                                    <small style={{fontSize: "14px"}} className="text-dark" style={{fontWeight: "500"}}><span className="text-muted">Department:</span> {this.state.dept_name}</small>
                                    <small style={{fontSize: "14px"}} className="text-dark mt-2" style={{fontWeight: "500"}}><span className="text-muted">Category:</span> {this.state.name}</small>
                                </div>}
                                <div className="card p-3" style={{background: "white"}}>
                                    <h5 className="font-weight-bold mt-1">Color</h5>
                                    <div className="row mt-2">
                                    {
                                        this.state.colors.map((color) => {
                                            return (
                                                <button key={color.attribute_value_id} 
                                                    className="btn ml-2 mb-2 text-center rounded-circle" 
                                                    style={{width: "25px", height: "25px", background: `${color.value}`}}>
                                                </button>
                                            )
                                        })
                                    }</div>
                                    <h5 className="font-weight-bold mt-3">Size</h5>
                                    <div className="row mt-2">
                                    {
                                        this.state.sizes.map((size) => {
                                            return (
                                                <button key={size.attribute_value_id} 
                                                    className="ml-2 mb-2 text-center btn-rec"
                                                    >
                                                    {size.value}
                                                </button>
                                            )
                                        })
                                    }</div>

                                    <div className="row mt-5 mx-auto">
                                        <button className="btn rounded px-3 text-light" style={{background: "#f52f5e", fontWeight: "500"}}>Apply</button>
                                        <small className="ml-3 text-center my-auto pointer" style={{color: "#f52f5e"}}>Clear All</small>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-8 mr-auto ml-5 text-center">
                                <div className="row">
                                {
                                    this.state.products.map((product) => {
                                        return (
                                            <div key={product.product_id} 
                                                className="col-lg-4">
                                                <div className="card mt-3 vikas box">
                                                    <div className="dim">
                                                    <img className="card-img-top p-3"  
                                                        alt="Card image cap" src={`https://raw.githubusercontent.com/zandoan/turing-frontend/master/Images/product_images/${product.thumbnail}`}/>
                                                    <div className="card-body text-center pt-0">
                                                        <p className="card-text font-weight-bold p-0">{product.name}</p>
                                                        <p className="card-text font-weight-bold p-0" style={{color: "#f52f5e"}}>${product.price}</p>
                                                        {/* <button type="button" 
                                                            className="btn px-3 mt-2 text-light rounded" 
                                                            style={{background: "#f52f5e", fontWeight: "500"}}
                                                            onClick={() => {
                                                                this.addToCart(product);
                                                            }}>Add To cart</button> */}
                                                    </div></div>
                                                    <div className="middle">
                                                        <div className="textvikas pointer"
                                                            onClick={() => {
                                                                //history.push(`/product-detail/${product.product_id}`)
                                                                history.push({
                                                                    pathname: `/product-detail/${product.product_id}`,
                                                                    state: { cartItems: this.state.cartItems }
                                                                });
                                                            }}>View</div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <CartModal cartItems={this.state.cartItems}/>
                <SignInModal />
                <SignUpModal />
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        //config: state.config,
        //auth: state.auth
        cart: state.cart
    };
};

const mapDispatchToProps = (dispatch) => ({
    setCartId: (cartId) => dispatch(setCartId(cartId))
});

//connect fn's parameter are for test cases
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(ProductPage);

export default ConnectedComponent;
