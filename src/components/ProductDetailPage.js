import React from 'react';
import {connect} from 'react-redux';
import Header from './Header';
import {getProductDetail, getAttributesValuesForProduct, 
        getProductReviewsApi, 
        generateCardId, addToCartApi} from "../services/categories";
import CartModal from './CartModal';
import SignInModal from './SignInModal';
import SignUpModal from './SignUpModal';

export class ProductDetailPage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            productDetail: undefined,
            attributes: [],
            itemCount: 0,
            reviews: [],
            cartId: undefined,
            cartItems: []
        }
    }

    componentDidMount() {
        const cartItems = this.props.location.state ? this.props.location.state.cartItems : []
        this.setState({cartItems: cartItems});

        const product_id = this.props.match.params.id
        getProductDetail(product_id)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("getProductDetail res", res);
            this.setState({productDetail: res});
            this.callToGetAttributeValuesForProduct(product_id);
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            //if(e.response && e.response.data) alert(e.response.data.msg)
        });
    }


    callToGetAttributeValuesForProduct = (product_id) => {
        //color attribute values
        getAttributesValuesForProduct(product_id)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("getAttributesValuesForProduct ", res);
            this.setState({attributes: res});
            this.getProductReviewDetail(product_id);
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        });
    }

    getProductReviewDetail = (product_id) => {
        //color attribute values
        getProductReviewsApi(product_id)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("getProductReviewsApi ", res);
            this.setState({reviews: res});
           // this.callToGetCartId();
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        });
    }
    
    /* callToGetCartId = () => {
        generateCardId()
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("generateCardId", res);
            this.setState({cartId: res.cart_id});
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        });
    } */

    addToCart = (product_id) => {
        addToCartApi(this.props.cart.cartId, product_id, "M")
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("addToCartApi response", res);
            this.setState({cartItems: res});
            alert("Item Added to Cart!");
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.error)
        });
    }

    updateCartItems = (items) => {
        this.setState({cartItems: items});
    } 

    render() {
        return (
            <div style={{background: "#efefef"}}>
                <Header cartItems={this.state.cartItems}/>
                <div style={{height: "91vh"}} >
                    <div className="container-fluid px-5" style={{background: "#efefef"}}>
                        {this.state.productDetail && <div className="card mt-4 mx-5">
                            <div className="row p-3">
                                <div className="col-md-4 text-center">
                                    <img src={`https://raw.githubusercontent.com/zandoan/turing-frontend/master/Images/product_images/`+ (this.state.productDetail[0].image)} 
                                        style={{width: "250px", maxHeight: "300px"}}
                                        className=""/>
                                </div>
                                <div className="col-md-8 pl-5">
                                    <div className="font-weight-bold text-muted">
                                        Home <span className="ml-4 mr-4"> • </span> Regional <span className="ml-4 mr-4"> • </span> French
                                    </div>
                                    <div className="mt-3">
                                        <span className="fa fa-star checkedStar"></span>
                                        <span className="fa fa-star checkedStar ml-1"></span>
                                        <span className="fa fa-star checkedStar ml-1"></span>
                                        <span className="fa fa-star ml-1"></span>
                                        <span className="fa fa-star ml-1"></span>
                                    </div>
                                    <h4 className="mt-4">{this.state.productDetail[0].name}</h4>
                                    <h4 style={{color: "#f52f5e"}}>£ {this.state.productDetail[0].price}</h4>
                                    <h5 className="font-weight-bold mt-4">Color</h5>
                                    <div className="row mt-2">
                                    {
                                        this.state.attributes.map((attribute) => {
                                            return attribute.attribute_name === "Color"  ? 
                                                <button key={attribute.attribute_value_id} 
                                                    className="btn ml-2 mb-2 text-center rounded-circle" 
                                                    style={{width: "25px", height: "25px", background: `${attribute.attribute_value}`}}>
                                                </button> : undefined
                                        })
                                    }</div>

                                    <h5 className="font-weight-bold mt-3">Size</h5>
                                    <div className="row mt-2">
                                    {
                                        this.state.attributes.map((attribute) => {
                                            return attribute.attribute_name === "Size"  ? 
                                                <button key={attribute.attribute_value_id} 
                                                    className="ml-2 mb-2 text-center btn-rec"
                                                    >
                                                    {attribute.attribute_value}
                                                </button>
                                               : undefined
                                        })
                                    }</div>

                                    {/* <div className="row mt-3">    
                                        <i className="fa fa-minus-circle text-center my-auto pointer" 
                                            style={{fontSize:"48px"}}
                                            onClick={() =>{
                                                if(this.state.itemCount>0){
                                                    this.setState({itemCount: --this.state.itemCount})
                                                }
                                            }}></i>
                                        <small className="ml-3 text-center my-auto" style={{fontSize: "25px"}}>{this.state.itemCount}</small>
                                        <i className="fa fa-plus-circle text-center my-auto ml-3 pointer" 
                                            style={{fontSize: "48px"}}
                                            onClick={() =>{
                                                this.setState({itemCount: ++this.state.itemCount})
                                            }}></i>
                                    </div> */}

                                    <button className="btn rounded px-5 text-light mt-5" 
                                        style={{fontSize: "25px", background: "#f52f5e", fontWeight: "500"}}
                                        onClick={() => {this.addToCart(this.state.productDetail[0].product_id)}}>Add to Cart</button>

                                </div>
                            </div>
                        </div>}
                        <div className="mt-5 mx-5">
                            <h3 >Product Reviews</h3>
                            {
                                this.state.reviews.map((review, index) => {
                                    return (
                                        <div key={index}>
                                            <div  className="row mt-3 ml-2">
                                                <div>
                                                    <div>
                                                        <span className="fa fa-star checkedStar"></span>
                                                        <span className="fa fa-star checkedStar ml-1"></span>
                                                        <span className="fa fa-star checkedStar ml-1"></span>
                                                        <span className="fa fa-star ml-1"></span>
                                                        <span className="fa fa-star ml-1"></span>
                                                    </div>
                                                    <h5 className="mt-3">{review.name}</h5>
                                                </div>
                                                <h5 className="ml-5" style={{fontWeight: "400"}}>{review.review}</h5>
                                            </div>
                                            <hr/>
                                        </div>
                                    )
                                })   
                            }            
                        </div>
                    </div>
                </div>
                <SignInModal />
                <SignUpModal />
                <CartModal cartItems={this.state.cartItems} updateCartItems={this.updateCartItems}/>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        //config: state.config,
        cart: state.cart
    };
};

const mapDispatchToProps = (dispatch) => ({
    //setConfigInfo: (config) => dispatch(setConfigInfo(config)),
});

//connect fn's parameter are for test cases
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(ProductDetailPage);

export default ConnectedComponent;
