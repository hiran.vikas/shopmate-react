import React from 'react';
import {connect} from 'react-redux';
import {history} from '../routers/AppRouter';
import {getCategories, getDepartments, getDepartmentsCats} from "../services/categories";
import {startLogout} from '../actions/auth';

export class Header extends React.Component {
    constructor(props){
        super(props); 
        
        this.state = {
            departments: [],
            categories: []
        }
    }

    componentDidMount(){
        /* getCategories()
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("getCategories", res);
            this.setState({categories: res.rows});
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        }); */
        getDepartments()
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("getDepartments", res);
            this.setState({departments: res}, () => {
                const array = this.state.departments
                this.getDeptCategories(array[0], 0);
                this.getDeptCategories(array[1], 1);
                this.getDeptCategories(array[2], 2);
            });
            
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        });
    }

    getDeptCategories = (department, index) => {
        getDepartmentsCats(department.department_id)
        .then((response) => {
            const res = response.data;
            if(process.env.DEBUG !== 'false') console.log("getDepartments cats", res);
            this.state.departments[index].categories = res;
            this.setState({departments: this.state.departments});
        })
        .catch((e) => {
            if(process.env.DEBUG !== 'false') console.log(e.response);
            if(e.response && e.response.data) alert(e.response.data.msg)
        });
    }


    getDeptCategoriesUI = (department) => {
        return department.categories.map((category) => {
            return  <li key={category.category_id}>
                        <a 
                            style={{color: "black", marginLeft: "8px"}}
                            onClick={() => {
                                console.log(history.location.pathname);
                                            history.push({
                                                pathname: `/products/${category.name}`,
                                                state: { category_id: category.category_id, 
                                                            category_name: category.name,
                                                            department_name: department.name }
                                            });
                                            window.location.reload();
                            }}>{category.name}</a>
                    </li>;
        });
    }

    onLogout = () => {
        this.props.startLogout();
        history.push('/');
        window.location.reload();
    }

    render(){   
        return(
            <nav className="navbar navbar-default navbar-expand-sm navbar-light navbarStyle" style={{background: "white"}}>
                <a className="navbar-brand pointer" onClick={(e) => {
                    e.preventDefault();
                    if(history.location.pathname !== '/'){
                        history.push("/");
                        window.location.reload();
                    }
                }}>
                    <div className="logo-header font-weight-bold">SHOPMATE </div>
                </a>
                {/* hide tab if from unitDetail page */}
                { 
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                }    
                {
                    <div className="collapse navbar-collapse ml-3" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                        {
                            this.state.departments.map((department) => {
                                return <li className="nav-item pointer ml-3 dropdown" key={department.department_id}>
                                    <a className="dept ml-2 dropdown-toggle"
                                        data-toggle="dropdown" 
                                        >{department.name}</a>
                                    <ul className="dropdown-menu">
                                        {department.categories && this.getDeptCategoriesUI(department)}
                                    </ul>
                                </li>
                            })
                        }
                        </ul>
                        {this.props.auth && this.props.auth.accessToken && <ul className="navbar-nav ml-auto">
                            <button type="button" className="btn text-dark border"
                                data-toggle="modal" data-target="#cartModal" >
                                Cart <span className="badge badge-danger">{this.props.cartItems? this.props.cartItems.length : 0}</span>
                                <span className="sr-only">unread messages</span>
                            </button>
                            <li className="nav-item pointer" >
                                <a className="nav-link text-dark">Hi {this.props.auth.customer.name}!</a>
                            </li>
                            <li className="nav-item pointer">
                                <a className="nav-link text-dark" onClick={this.onLogout}>Sign Out</a>
                            </li>
                        </ul>}
                        {!this.props.auth || !this.props.auth.accessToken && <ul className="navbar-nav ml-auto">
                            <button type="button" className="btn text-dark border"
                                data-toggle="modal" data-target="#cartModal" >
                                Cart <span className="badge badge-danger">{this.props.cartItems? this.props.cartItems.length : 0}</span>
                                <span className="sr-only">unread messages</span>
                            </button>
                            <li className="nav-item pointer" data-toggle="modal" data-target="#signInModal">
                                <a className="nav-link text-dark">Log in</a>
                            </li>
                            <li className="nav-item pointer" data-toggle="modal" data-target="#signUpModal">
                                <a className="nav-link text-dark">Sign Up</a>
                            </li>
                        </ul>}
                    </div>
                }
            </nav>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }    
};

const mapDispatchToProps = (dispatch) => ({
    startLogout: () => dispatch(startLogout()),
});

//connect fn's parameter are for test cases
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(Header);

export default ConnectedComponent;