export const setUserInfo = (res) => ({
    type: 'SET_AUTH_USER',
    res
});


export const startLogout = () => ({
    type: 'LOGOUT'
});
