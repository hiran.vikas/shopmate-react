export const setCartId = (cartId) => ({
    type: 'SET_CART_ID',
    cartId
});

export const addItemQuantity = (item) => ({
    type: 'ADD_ITEM_QUANTITY',
    item
});

export const removeItemQuantity = () => ({
    type: 'REMOVE_ITEM_QUANTITY',
    item
});

export const startLogout = () => ({
    type: 'LOGOUT'
});
