import React from 'react';
import {connect} from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

//Destructure props here i.e isAuthenticated from mapStateToProps
// then 'component' from AppRouter file from 'PrivateRoute'
// and other props i.e exact, path will be like this i.e spread out rest ...rest

// Here what we doing is if isAuthenticated then Route to correct component else to root i.e(/)
export const PrivateRoute = ({
        auth, 
        component: Component, 
        ...rest
    }) => (
    <Route {...rest} component={(props) => (
        auth && auth.user && auth.jwt  ? (
            <div>
                <Component {...props}/>
            </div>
        ) : (
            <Redirect to="/" />
        )
    )}/>
); 


const mapStateToProps = (state) => ({
    auth: state.auth
});

export default connect(mapStateToProps)(PrivateRoute);