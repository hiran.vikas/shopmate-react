import React from 'react';
import {connect} from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

export const PublicRoute = ({
        auth, 
        component: Component, 
        ...rest
    }) => (
    <Route {...rest} component={(props) => (
        !(auth && auth.user && !auth.user.is_email_verified) ? (
            <Redirect to="/" />
        ) : (
            <Component {...props}/>
        )
    )}/>
); 


const mapStateToProps = (state) => ({
    auth: state.auth
});

export default connect(mapStateToProps)(PublicRoute);