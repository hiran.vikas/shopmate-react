import React from 'react';
import {Router, Route, Switch} from 'react-router-dom';

//import createHistory from 'history/createBrowserHistory';
import { createBrowserHistory } from "history";
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

import DashBoard from '../components/DashBoard';
import ProductPage from '../components/ProductPage';
import ProductDetailPage from '../components/ProductDetailPage';
import NotFoundPage from '../components/NotFoundPage';

export var history = createBrowserHistory();

//Router Component ==> export this
export const AppRouter = () => (
    <Router history={history}>
        <div>
            <Switch>
                <Route path="/" component={ProductPage} exact={true}/>
                <Route path="/products/:id" component={ProductPage} exact={true}/>
                <Route path="/product-detail/:id" component={ProductDetailPage} exact={true}/>
                <Route component={NotFoundPage}/>
            </Switch>
        </div> 
    </Router>
);