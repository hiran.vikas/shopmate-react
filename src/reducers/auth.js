export default (state = {}, action) => {
    switch(action.type){
        case 'SET_AUTH_USER':
            return action.res;
        case 'LOGOUT':
            return {};
        default:  return state;
    }
};