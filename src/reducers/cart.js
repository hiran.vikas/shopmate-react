export default (state = {}, action) => {
    switch(action.type){
        case 'SET_CART_ID':
            return {cartId: action.cartId};
        case 'LOGOUT':
            return {};
        default:  return state;
    }
};