import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {AppRouter, history} from './routers/AppRouter';
import configureStore from './store/configureStore';
import {loadState, saveState} from './localStorage'

import 'normalize.css/normalize.css';
import './styles/styles.scss';
import "react-datepicker/dist/react-datepicker.css";


const persistedState = loadState();
const store = configureStore(persistedState);

//Things you want to be persisted all time
store.subscribe(() => {
  saveState({
      auth: store.getState().auth,
      cart: store.getState().cart
  });
});

const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);


let hasRendered = false;
const renderApp = () => {
  if(!hasRendered) {
    ReactDOM.render(jsx, document.getElementById('app'));
    hasRendered = true;
  }
};
renderApp();
