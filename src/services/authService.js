import {getAxios} from './axiosUtil';

export const signupApi = (name, email, password) => {
    var formdata = {
        name: name,
        email: email ,
        password: password
    }
    return getAxios().post(`${process.env.BASE_API_URL}/customers`, formdata);
};

export const signInApi = (email, password) => {
    var formdata = {
        email: email ,
        password: password
    }
    return getAxios().post(`${process.env.BASE_API_URL}/customers/login`, formdata);
};

