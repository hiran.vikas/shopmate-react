import axios from 'axios';

const UNAUTHORIZED = 401;
import configureStore from '../store/configureStore';
import {loadState, saveState} from '../localStorage'
import {history} from '../routers/AppRouter';


export const getAxios = () => {
    //console.log("interceptors: ", axios.interceptors);
    if(axios.interceptors.response.handlers.length === 0){
        axios.interceptors.response.use(
        response => response,
        error => {
            const {status} = error.response;
            //console.log("***status: ", status);
            /* if (status === UNAUTHORIZED) {
                //console.log("****logged out, token expired in showing service");
                    //alert("HI loggeing out");
                    const persistedState = loadState();
                    const store = configureStore(persistedState);
                    store.dispatch(startLogout());
                    saveState({
                        auth: {}
                    })
                    if(history.location.pathname === '/'){
                        window.location.reload();
                    }
                    else{
                        history.push('/');
                    }
                return Promise.reject(error);   
            } */
            return Promise.reject(error);
        }
        );
    }
    return axios;
 };
 