import {getAxios} from './axiosUtil';

export const getDepartments = () => {
    return getAxios().get(`${process.env.BASE_API_URL}/departments`);
};

export const getDepartmentsCats = (id) => {
    return getAxios().get(`${process.env.BASE_API_URL}/categories/inDepartment/${id}`);
}

export const getCategories = () => {
    return getAxios().get(`${process.env.BASE_API_URL}/categories`);
};

export const getProductListByCategory = (category_id) => {
    return getAxios().get(`${process.env.BASE_API_URL}/products/inCategory/${category_id}`);
}

export const getAttributes = () => {
    return getAxios().get(`${process.env.BASE_API_URL}/attributes`);
}

export const getAttributesValues = (attr_id) => {
    return getAxios().get(`${process.env.BASE_API_URL}/attributes/values/${attr_id}`);
}

export const getAttributesValuesForProduct =  (product_id) => {
    return getAxios().get(`${process.env.BASE_API_URL}/attributes/inProduct/${product_id}`);
}

export const generateCardId = () => {
    return getAxios().get(`${process.env.BASE_API_URL}/shoppingcart/generateUniqueId`);
}

export const addToCartApi = (cartId, productId, attributes) => {
    console.log("cart id: ", cartId);
    console.log("product id: ", productId);
    console.log("attribute: ", attributes);

    //json format
    var formdata = {
        cart_id: cartId,
        product_id: productId,
        attributes: attributes
    }
    return getAxios().post(`${process.env.BASE_API_URL}/shoppingcart/add`, formdata);
}

export const getProductDetail = (product_id) => {
    return getAxios().get(`${process.env.BASE_API_URL}/products/${product_id}/details`);
}

export const getProductReviewsApi = (product_id) => {
    return getAxios().get(`${process.env.BASE_API_URL}/products/${product_id}/reviews`);
}

export const getCartListItems = (cart_id) => {
    return getAxios().get(`${process.env.BASE_API_URL}/shoppingcart/${cart_id}`);
}

export const updateCartItemsApi = (item) => {
    var formdata = {
        item_id: item.item_id,
        quantity: item.quantity
    }
    return getAxios().put(`${process.env.BASE_API_URL}/shoppingcart/update/${item.item_id}`, formdata);
}


export const removeItemFromCartApi = (item) => {
    var formdata = {
        item_id: item.item_id,
    }
    return getAxios().delete(`${process.env.BASE_API_URL}/shoppingcart/removeProduct/${item.item_id}`, formdata);
}
