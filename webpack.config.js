const path = require('path');
const webpack = require('webpack');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
//var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = (env) => {
    const CSSExtract = new MiniCssExtractPlugin({ filename: 'styles.css' });
    const CompressionPlugin = require('compression-webpack-plugin');

    process.env.NODE_ENV = process.env.NODE_ENV || 'development';

    const isProduction = process.env.NODE_ENV === 'production';
    console.log("is Production", isProduction);
    
    if(process.env.NODE_ENV === 'development') {
        require('dotenv').config({path: '.env.development'});
    }
    else{
        require('dotenv').config({path: '.env'});
    }

    return {
        entry: ['babel-polyfill','./src/app.js'],
        output: {
            path: path.join(__dirname, 'public', 'dist'),
            filename: 'bundle.js'
        },
        module: {
            rules:[{
                loader: 'babel-loader',
                test:/\.js$/,
                exclude: /node_modules/
            }, {
                test:/\.s?css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            }]
        },
        plugins: [
            CSSExtract,
            new webpack.DefinePlugin({
                'process.env.BASE_API_URL':JSON.stringify(process.env.BASE_API_URL),
                'process.env.DEBUG':JSON.stringify(process.env.DEBUG)/*,
                 'process.env.GOOGLEMAP_API_KEY':JSON.stringify(process.env.GOOGLEMAP_API_KEY) */
            }),
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
            new CompressionPlugin()/* ,
            new BundleAnalyzerPlugin() */
        ],
        devtool: isProduction? /* 'source-map' */undefined : 'inline-source-map',
        devServer: {
            contentBase: path.join(__dirname, 'public'),
            historyApiFallback: true,
            publicPath: '/dist/'
        }
    };
}
