const path = require('path');
var express = require('express');
var app = express();
const publicPath = path.join(__dirname, '..', 'public');
const port = process.env.PORT || 3000; //for getting dynamic PORT for heroku,3000 for local machine

app.use(express.static(publicPath));

app.get('*',(req, res) => {
    res.sendFile(path.join(publicPath, 'index.html'));
})

//3000 is port where our app will show up
app.listen(port, () => {
        console.log('Express Server is up')
})
