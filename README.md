# rentloop-react

Shopmate app deployed on heroku. Following is the link : 

[Shopmate deployed here](https://shopmate-vikas-hiran.herokuapp.com)


The core libraries are as follows:

* React 

* Redux 

* Bootstrap4 

* Axios for network calls 

I haven't used your code as It was difficult to understand. So I created my own react poject and deployed it on heroku platform.
Itmay not be exact like your template did it my way!


**Functionalities Implemented:**
1) Department and their respected categories fetched and shown on  Header.
2) Filters shown on Home page as I can not find api for filters so left as it is! It wont work!
3) Fetch products according to department > categories.
4) Sign in functionality
5) Register functionality
6) Product detail Api 
7) Get Reviews Api
8) Add to cart Api
9) Cart Screen (Modal)
10) add / remove items from cart
11) completely remove item from cart

As days were less so tried to build few functionalities. 


**Looking forward for your positive response!**


**Scripts to Note**

`yarn install`  to install all libraries (node modules folder)


`yarn dev-server`  -> to start app

Runs the app in the development mode.
Open http://localhost:8080 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.


And if you can not run app with above commands then try to visit bellow url where I have deployed shopmate app.

(https://shopmate-vikas-hiran.herokuapp.com)



